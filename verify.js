import { checkCondition } from "verifiable-credention-check-library";

const someCondition = {
  id: "https://example.com/someCondition",
  predicateURL: "https://example.com/livesInfrance",
  // Can be the dateUser itself, if he has to agree to some conditions
  authority: "https://france.fr/signature"
}

const DataSet = {
  id: "uri:someId"
  ...
  conditions: [
    ...
    someCondition
  ]
}

checkCondition(someCondition) => true

checkCondition(userInput) => true
