import { signCredential } from "verifiable-credential-sign-library";

const attributeSet = {
  livesInFrane: true,
}

const authority = {
  publicKey: "AB06251056156FE2713589",
  signURL: "https://example.com/someOrg/publickey"
}

signCredential(attributeSet, authority) => ?
