# Datadeal Protocol

The Datadeal Protocol (`datpro`) is a specification for creating agreements between two parties about data usage. It standardizes the process of specifying conditions for sharing, signing the deal, validating signatures and granting access to the resource.

## Terminology

- **(Data) User**: The agent (often person) who wants to use some Resource. Specifically often the _browser client_.
- **(Data) Subject**: The piece of data (a file, RDF resource or even API) that is granted access to.
- **(Data) Owner**: The agent (often person) who is in possessions and legal control of the Resource.
- **(DataDeal) Offer (n)**: A Resource that describes the conditions under which some Data Owner would like to share some Resource. (n) describes the amount of nested signatures.
- **(DataDeal) Agreement**: The finalized, signed and approved resource that describes
- **Resource**: An RDF-compatible linked data resource. A file containing triples.
- **Pod**: A DexPod or Solid Pod
- **Broker**: A web service that acts as a trusted third party in a DataDeal. It typically checks the signatures, provides a third signature and persists the signed Agreement.

# First architecture

## Standard Flow

The standard flow describes a Data User who owns a WebID and wants to access some public resource that requires a DataDeal Agreement before access is provided.

- User sends HTTP GET request to Data Owner's Pod (DOP) Resource URL `https://joepio.dexpods.eu/my_resources/123`.
- Pod responds with a 401 (Authorization Required) (or maybe redirect to?). The response contains a DataDeal Offer (1) in the Body. It is signed by the Data Owner's private key.
- The User views the DataDeal Offer, and is free to agree to it. If the User Agrees, the N-Triples serialized Offer is signed using the private key of the User. This is a DataDeal Offer (2) with a _second_ signature.
- If the DataDeal Offer has a Broker (a `dadpro:broker` triple), it is sent to that broker (POST DataDeal Offer(2) to `broker` URL). The Broker can perform extra checks, such as payment requirements.
- The Broker checks the DataDeal Offer(2):
  - The webID of the User is fetched and the corresponding `datpro:publicKey` is found. The Broker checks if this corresponds to the `datpro:userSignature` of the Offer.
  - Same is done for the Data Owner and the `datpro:ownerSignature`.
  - If there is a Payment requirement (a `datpro:price` is set), the Broker responds with a

# Second architecture

- Owner posts Offer to broker
- Owner posts Signature to broker
- User gets resource, get's redirected to Offer on Broker
- User gets Offer from Broker
- If Broker requires signature: user signs Offer using Pod (posts to `/sign` endpoint? Redirects with result?)
- User posts Signature to broker
-
