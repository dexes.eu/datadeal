# DataDeal Specification - open standard for exchanging data on the internet

_Status: Draft_
_Version: 0.3.1_

## Development halted

Development of this spec is halted in favor of using the [W3C ODRL spec](https://www.w3.org/TR/odrl-model/).

## Intro

The DataDeal (dd) specification aims to make it easier for data owners to specify under which conditions data(sets) can be shared.
It is machine readable, and uses cryptography ([Verifiable Credentials](https://www.w3.org/TR/vc-data-model/)) to make sure that users actually have the right Properties to fulfill the conditions set by the data owner.

- Graph IRI: http://purl.org/dad
- Preferred prefix: `dad`

## Status

The current version is *draft* and is therefore prone to breaking changes.
We expect a more stable version in Q3 2020.

## Purpose of this spec

Make it easier to create machine-readable, executable agreements on data sharing.

1. Which *dataset*
1. Who *owns* the data
1. Who will *use* the data
1. Under what *conditions* can the data be shared

## Usecase Examples

1. Andy has a dataset about plane emissions in France. He creates a `dad:Offering` that specifies who can access it. He only wants to share it with people with French nationality, so he uses a `dad:Condition` that specifies how this is validated. Barbera wants to use the plane emissions dataset, and must comply to the conditions in the `dad:Offering`. Barbera asks her government for a `dad:Validation` and saves it. She signes the `dad:Offering`, and  data deal, which includes her `dad:Validation`.

## Classes

The following items are instances of `rdf:Class`.

### dad:Offering

The Offering describes which DataSet is to be shared, by whom, under which conditions.

Properties:

- dad:dataOwner (the one sharing the data, WebID)
<!-- - dad:dataUser (optional, the one who wants to use the shared data) -->
- dad:transferrable (dad:Transferrable, probably)
- dad:license (preferably a dad:License)
- dad:dateSigned
- dad:maxValidDuration
- dad:maxRequestsPerMinute

### dad:License

Properties:

- dad:conditions - A list of non-verifiable Conditions (often textually described)
- dad:verfiableConditions - A list of executable

### dad:Transferrable

A resource (such as a file, or a dataset) that can be transferred.

Possible subclasses: dcat:DataSet

### dad:DataDeal

The DataDeal is an bilateral agreement between two parties that specifies how a dataset is to be used.
It is a `dad:Offering` with, at the very least, a signatures of the `dad:dataUser`.
It should be crytographically signed by the dataUser and dataOwner.

Properties:

- dad:dataUser
- dad:dataOwner
- dad:validations
- dad:transferrable (object dat verkocht wordt, dataset bijv.)
- dad:dcat
- dad:dateSigned
- dad:dataSpace (should be a Versioned resource, maybe use IPFS?)

### dad:Condition

A Condition is a clearly defined module inside of a single License.
It is a part of a License.
Note: even though Conditions are not Verfiable in the same way that VarifiableConditions are, we can still create a crytographical signature that proves that some User has agreed to the Condition.

Example:

- "Share alike: Requires "

Properties:

- schema:name
- schema:text
- dad:icon

### dad:VerifiableCondition

A VerifiableCondition describes an attribute that is required by the data owner for the data user to have.
VerifiableConditions are proven cryptographically using the [Verifiable Credential](https://www.w3.org/TR/vc-data-model/) specification.

Examples:

- dataUser must live in France, authorized by the French national government.
- dataUser is Person X

Properties:

- dad:requiredPredicate
- dad:requiredValue
- dad:requiredSigner
- dad:maxValidDuration

### dad:Validation

A filled in `dad:VerifiableCondition`, which contains the result of the check.

Properties:

- dad:validUntil
- dad:validatesVerifiableCondition
- dad:signedBy

### dad:ValidationRequest

An object that is sent to some authority that describes how a certain agent requests som verified credential

_perhaps the Verifiable Credentials spec covers this?_

### dad:DataSpace

A DataSpace is a

## Properties

The following items are instances of `rdf:Property`.

### dad:dataOwner

The person or organization that owns the `dad:transferrable`.

Value: a `foaf:Agent`

### dad:dataUser

The person or organization that will use the `dad:transferrable`.

Value: a `foaf:Agent`

## dad:maxRequestsPerMinute

Specifies the number of requests that the data owner should be able to handle.
If the user exceeds this amount, the dataOwner's server should respond with 429 "Too many requests".

Value: `xsd:number`

### dad:transferrable

The data item that is transferred, e.g. the dataset.

Value: URL of any `dcat:DataSet`

### dad:requiredPredicate

The predicate (key) of the data that is to be validated.
Example: `https://schema.org/birthdate`

Value: URL of any ``

### dad:requiredValue

The shape of the value that is required.
This can be declared using the SHACL specification.

Value: URL of a SHACL property shape:

_Maybe_ simply require a 'true' attribute at first.

### dad:requiredSigner

The URL of the authority that should verify the attribute.

_example: The municipality verifies the _

### dad:dateSigned

### dad:hasVerifiableConditions

A set of `dad:VerifiableCondition` that the User has to fulfill before the `dad:DataDeal` can be made.

Value: URL of an `rdf:List` of `dad:VerifiableCondition` items.

### dad:maxValidDuration

### dad:validations

The set of validations (`dad:VerifiableCondition` which are met) for the Offering.

Value: URL of an `rdf:List` of `dad:Validations`.

## Flow

1. User wants to use some Dataset. The DataSet has a `dad:hasOffering` reference to a specific `dad:Offering`.
1. The User downloads the specific `dad:Offering`
1. The User's agent makes sure that the required `dad:validations` are met. If all are met, skip to #x.
1. The User's agent sends a `dad:ValidationRequest` to an authority to sign some credential.
1. The User's agent adds the `dad:Validations` and makes sure they are available to the data owner.
